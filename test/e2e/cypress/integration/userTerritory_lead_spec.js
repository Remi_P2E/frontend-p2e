describe('User territory - lead', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
    cy.fixture('territories').as('territories')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport list for territory users', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'userterritory1', data: this.users.userterritory1, creds: this.creds.userterritory1 },
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.leadPantin }
        ]
      },
      {
        type: 'territories',
        items: [
          { name: 'territory1', data: this.territories.territory1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Assigning leads")
    cy.login(this.creds.auditor1)
    cy.getCreatedItemByName('lead1').then(lead => {
      cy.assignLead(lead)
    })

    cy.log("# Defining test territory")
    cy.login(this.creds.admin)
    cy.getCreatedItemByName(['territory1', 'userterritory1']).then(createdItems => {
      cy.assignCityToTerritory(createdItems.territory1, 35357)
      cy.assignUserToTerritory(createdItems.territory1, createdItems.userterritory1)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")

    cy.login(this.creds.userterritory1)
    cy.getCreatedItemByName('lead1').then(lead => {
      cy.visit('/#/admin/lead?lead=' + lead.id)
    })

    cy.wait('@getLead')
    cy.get('[data-cy=lead-address] :input').should('have.value',
      this.users.particulier1.address.address
      + ', ' + this.users.particulier1.address.zipcode
      + ' ' + this.users.particulier1.address.city
    )
    cy.contains('Je reconnais avoir pris connaissance du règlement de l’inscription à l\'expérimentation du P2E et accepte ses conditions')

    cy.get('[data-cy="lead-btn-submit"]').should('not.exist')

  })
})
