
describe('User create Lead', function () {

  before(function () {
    cy.serverStart()
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
  })

  beforeEach(function () {
    cy.logout()
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('User can create a lead', function () {

    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      }
    ]
    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.particulier1)
    cy.visit('/#/admin/lead')

    cy.get('[data-cy="lead-address"] :input').type(this.users.particulier1.address.address, { force: true })
    cy.wait('@searchAddress')
    cy.get(':nth-child(1) > .q-item-main').click()

    cy.contains('Négociant et distributeur').click()
    cy.contains('Je reconnais avoir pris connaissance du règlement de l’inscription à l\'expérimentation du P2E et accepte ses conditions').click()
    cy.contains('Je certifie sur l\'honneur être propriétaire d\'une résidence principale de type maison individuelle, construite avant 1990').click()

    cy.get('[data-cy="lead-comments"] .q-input-area').type("No comments", { force: true })

    cy.get('[data-cy="lead-btn-submit"]').click()
    cy.wait('@createLead').then(function (xhr) {
      expect(xhr.status).equals(201)
    })

  })
})
