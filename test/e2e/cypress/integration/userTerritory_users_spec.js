describe('User territory - users', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
    cy.fixture('territories').as('territories')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport list for territory users', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'userterritory1', data: this.users.userterritory1, creds: this.creds.userterritory1 },
          { name: 'userterritory2', data: this.users.userterritory2, creds: this.creds.userterritory2 },
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'territories',
        items: [
          { name: 'territory1', data: this.territories.territory1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Defining test territory")
    cy.login(this.creds.admin)
    cy.getCreatedItemByName(['territory1', 'userterritory1', 'userterritory1']).then(createdItems => {
      cy.assignCityToTerritory(createdItems.territory1, 35357)
      cy.assignUserToTerritory(createdItems.territory1, createdItems.userterritory1)
      cy.assignUserToTerritory(createdItems.territory1, createdItems.userterritory1)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")

    cy.login(this.creds.userterritory2)
    cy.visit('/#/admin/users')
    cy.contains('Aucun utilisateur')
    cy.get('tr').should('have.length', 1)

    cy.login(this.creds.userterritory1)
    cy.reload(true)
    cy.contains(this.users.particulier1.firstname)
    cy.get('tr').should('have.length', 2)

    cy.getCreatedItemByName('particulier1').then(user => {
      cy.get('[data-cy=users-edit-id-' + user.id + ']').should('not.exist')
    })

  })
})
