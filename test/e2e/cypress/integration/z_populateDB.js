before(function () {
  cy.serverStart()
  cy.fixture('leads').as('leads')
  cy.fixture('territories').as('territories')
  cy.fixture('creds').as('creds')
  cy.fixture('users').as('users')
  cy.fixture('passports').as('passports')
  cy.fixture('renovationPlans').as('renovationPlans')
})

beforeEach(function () {
  cy.log("## CLEANING DB BEFORE TESTS")
  cy.cleanAll()
})

it('Test renovation plan readable and updatable for auditor', function () {
  var itemsToCreate = [
    {
      type: 'users',
      items: [
        { name: 'adminterritory1', data: this.users.adminterritory1, creds: this.creds.adminterritory1 },
        { name: 'adminterritory2', data: this.users.adminterritory2, creds: this.creds.adminterritory2 },
        { name: 'userterritory1', data: this.users.userterritory1, creds: this.creds.userterritory1 },
        { name: 'userterritory2', data: this.users.userterritory2, creds: this.creds.userterritory2 },
        { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
        { name: 'auditor2', data: this.users.auditor2, creds: this.creds.auditor2 },
        { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 },
        { name: 'particulier2', data: this.users.particulier2, creds: this.creds.particulier2 }
      ]
    },
    {
      type: 'territories',
      items: [
        { name: 'territory1', data: this.territories.territory1 },
      ]
    },
    {
      type: 'leads',
      as: this.creds.particulier1,
      items: [
        { name: 'lead1', data: this.leads.lead1 },
        { name: 'lead2', data: this.leads.lead2 },
        { name: 'lead3', data: this.leads.lead3 }
      ]
    },
    {
      type: 'leads',
      as: this.creds.particulier2,
      items: [
        { name: 'lead4', data: this.leads.lead1 },
        { name: 'lead5', data: this.leads.lead2 },
        { name: 'lead6', data: this.leads.lead3 }
      ]
    },
    {
      type: 'leads',
      as: this.creds.auditor1,
      items: [
        { name: 'lead7', data: this.leads.lead4, owner: 'particulier1' },
        { name: 'lead8', data: this.leads.lead5, owner: 'particulier2' }
      ]
    },
    {
      type: 'passports',
      as: this.creds.auditor1,
      items: [
        { name: 'passport1', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead1' },
        { name: 'passport2', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead2' },
        { name: 'passport3', data: this.passports.passport1, user: this.users.particulier2, leadName: 'lead4' },
        { name: 'passport4', data: this.passports.passport1, user: this.users.particulier2, leadName: 'lead5' },
        { name: 'passport5', data: this.passports.passport1, user: this.users.particulier2, leadName: 'lead7' }
      ]
    },
    {
      type: 'renovationPlans',
      as: this.creds.auditor1,
      items: [
        { name: 'plan1', data: this.renovationPlans.plan1, passportName: 'passport1' },
        { name: 'plan2', data: this.renovationPlans.plan1, passportName: 'passport3' },
        { name: 'plan3', data: this.renovationPlans.plan1, passportName: 'passport5' }
      ]
    }
  ]

  cy.log("##POPULATING DB")
  cy.populateDb(itemsToCreate)
})