describe('Admin - leads', function () {

  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test leads list display as admin', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'auditor2', data: this.users.auditor2, creds: this.creds.auditor2 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.lead1 },
          { name: 'lead2', data: this.leads.lead2 },
          { name: 'lead3', data: this.leads.lead3 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Assigning leads")
    cy.login(this.creds.auditor1)
    cy.getCreatedItemByName('lead1').then(response => {
      cy.assignLead(response)
    })
    cy.getCreatedItemByName('lead2').then(response => {
      cy.assignLead(response)
    })
    cy.login(this.creds.auditor2)
    cy.getCreatedItemByName('lead3').then(response => {
      cy.assignLead(response)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.admin)
    cy.visit('/#/admin/leads')

    cy.contains('tr', 'Nom').should('be.visible')
    cy.contains('tr', 'Prénom').should('be.visible')
    cy.contains('tr', 'Email').should('be.visible')
    cy.contains('tr', 'Adresse').should('be.visible')
    cy.contains('tr', 'Code postal').should('be.visible')
    cy.contains('tr', 'Ville').should('be.visible')
    cy.contains('tr', 'Assignée').should('be.visible')
    cy.contains('tr', 'Actions').should('be.visible')

    cy.get('[data-cy="leads-search"] :input').clear({ force: true }).type(this.leads.lead1.address.address, { force: true })
    cy.contains(this.creds.particulier1.username).should('be.visible')
    cy.contains(this.leads.lead1.address.address).should('be.visible')

    cy.get('[data-cy="leads-search"] :input').clear({ force: true }).type(this.leads.lead2.address.address, { force: true })
    cy.contains(this.creds.particulier1.username).should('be.visible')
    cy.contains(this.leads.lead2.address.address).should('be.visible')

    cy.get('[data-cy="leads-search"] :input').clear({ force: true }).type(this.leads.lead2.address.address, { force: true })
    cy.contains(this.creds.particulier1.username).should('be.visible')
    cy.contains(this.leads.lead2.address.address).should('be.visible')
  })
})
