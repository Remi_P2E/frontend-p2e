import { auditorConnection, adminConnection, mountWithRoutes } from '@test/helpers/helpers.js'
import Home from '@/admin/Home.vue'
import ConnectedLayout from '@/layouts/ConnectedLayout.vue'
import expect from 'expect'

const routes = [
  { path: '/', component: Home }
]

describe('Home.vue', () => {
  const wrapper = mountWithRoutes(ConnectedLayout, routes)
  it('Page should render exactly all the category link provided', () => {
    expect(wrapper.findAll('.category-link').length).toBe(1)
  })
  it('Page should render exactly all the buttons in toolbar', () => {
    expect(wrapper.findAll('.q-toolbar button').length).toBe(2)
  })
})

describe('Home.vue as Admin', () => {
  const wrapper = mountWithRoutes(ConnectedLayout, routes, adminConnection)
  it('Page should render exactly all the category link provided', () => {
    expect(wrapper.findAll('.category-link').length).toBe(8)
  })
  it('Page should render exactly all the buttons in toolbar', () => {
    expect(wrapper.findAll('.q-toolbar button').length).toBe(2)
  })
})

describe('Home.vue as Auditor', () => {
  const wrapper = mountWithRoutes(ConnectedLayout, routes, auditorConnection)
  it('Page should render exactly all the category link provided', () => {
    expect(wrapper.findAll('.category-link').length).toBe(6)
  })
  it('Page should render exactly all the buttons in toolbar', () => {
    expect(wrapper.findAll('.q-toolbar button').length).toBe(2)
  })
})
