import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const VentsFormOptions = {
  glazingType: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true },
    { value: 'SIMPLE_GLAZING', label: 'Simple-vitrage' },
    { value: 'OLD_DOUBLE_GLAZING', label: 'Double-vitrage ancien' },
    { value: 'DOUBLE_GLAZING', label: 'Double-vitrage' },
    { value: 'HIGH_PERFORMANCE_DOUBLE_GLAZING', label: 'Double-vitrage Haute Performance' },
    { value: 'TRIPLE_GLAZING', label: 'Triple-vitrage' },
    { value: 'DOUBLE_WINDOW', label: 'Double fenêtre' }
  ],
  // WARNING: this enumeration is used both for the passport display and the vp conditions values
  poseType: [
    { value: 'RABBET', label: 'Feuillure' },
    { value: 'INDOOR_WALL_MOUNTED', label: 'Applique intérieur' },
    { value: 'TUNNEL', label: 'Tunnel' },
    { value: 'OUTDOOR_WALL_MOUNTED', label: 'Applique extérieur' }
  ]
}

export default {
  glazingType: convertFormOptionsToEnum(VentsFormOptions.glazingType),
  poseType: convertFormOptionsToEnum(VentsFormOptions.poseType)
}
