import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const RoofFormOptions = {
  atticType: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true },
    { value: 'LOST_ATTIC', label: 'Combles perdues' },
    { value: 'RAKE', label: 'Rampants' },
    { value: 'PATIO', label: 'Terrasse' }
  ]
}

export default {
  atticType: convertFormOptionsToEnum(RoofFormOptions.atticType)
}
