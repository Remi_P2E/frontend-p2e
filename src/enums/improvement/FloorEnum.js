import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const FloorFormOption = {
  type: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true },
    { value: 'OTHER_UNHEATED_SPACE', label: 'Autre local non chauffé' },
    { value: 'TERREPLEIN', label: 'Terre-plein' },
    { value: 'CRAWL_SPACE', label: 'Vide-sanitaire' }
  ]
}

export default {
  type: convertFormOptionsToEnum(FloorFormOption.type)
}
