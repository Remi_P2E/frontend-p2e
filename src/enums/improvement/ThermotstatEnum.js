import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const ThermostatOptions = {
  values: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true }
  ]
}

export default {
  thermostats: convertFormOptionsToEnum(ThermostatOptions.values)
}
