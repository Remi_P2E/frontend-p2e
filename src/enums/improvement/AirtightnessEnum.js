import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const AirtightnessEnum = {
  values: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true, ignoreInEnum: true },
    { value: 'BAD', label: 'Mauvaise', 'preceding': true, 'following': false, ignoreInEnum: true },
    { value: 'AVERAGE', label: 'Moyenne', 'preceding': true, 'following': false, ignoreInEnum: true },
    { value: 'GOOD', label: 'Bonne', 'preceding': true, 'following': true },
    { value: 'VERY_GOOD', label: 'Très bonne', 'preceding': true, 'following': true }
  ]
}

export default {
  airtightness: convertFormOptionsToEnum(AirtightnessEnum.values)
}
