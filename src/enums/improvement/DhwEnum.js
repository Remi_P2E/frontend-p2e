import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const DhwOptions = {
  solar: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true },
    { value: 1, label: 'Solaire', 'preceding': true, 'following': true },
    { value: 0, label: 'Non solaire', 'preceding': true, 'following': true }
  ]
}

export default {
  dhw: convertFormOptionsToEnum(DhwOptions.solar)
}
