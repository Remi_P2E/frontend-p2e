function selectOptions (rawOpts) {
  if (arguments.length !== 1) {
    return selectOptions(arguments)
  }
  if (rawOpts instanceof Array) {
    return rawOpts.map(opt => { return opt instanceof Object ? opt : { label: opt.toString(), value: opt } })
  } else {
    return Object.keys(rawOpts).map(k => { return { label: rawOpts[k], value: k } })
  }
}

class SelectOptions {
  install (Vue) {
    Vue.$selectOptions = Vue.prototype.$selectOptions = selectOptions
  }
}

export default new SelectOptions()
