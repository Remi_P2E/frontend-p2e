export default class ApiCollection {
  constructor (type, Api) {
    this.__type = type
    this.__api = Api
  }

  find (params = {}) {
    return this.__api.get(this.__type + '/', params)
      .then(response => {
        return response.body
      })
  }

  get (id) {
    return this.__api.get(this.__type + '/' + id)
      .then(response => {
        return response.body
      })
  }

  create (data) {
    return this.__api.post(this.__type + '/', data)
      .then(response => {
        return response.body
      })
      .catch(response => {
        throw response.body
      })
  }

  update (id, data) {
    return this.__api.patch(this.__type + '/' + id, data)
      .then(response => {
        return response.body
      })
      .catch(response => {
        throw response.body
      })
  }

  delete (id) {
    return this.__api.delete(this.__type + '/' + id)
      .then(response => {
        return response.body
      })
      .catch(response => {
        throw response.body
      })
  }
}
