export default class ApiAuthentication {
  constructor (localStorage) {
    this.$localStorage = localStorage
    this.__set(null)
  }

  __set (jwt, time) {
    if (jwt) {
      this.jwt = jwt
      let parts = jwt.split('.')
      this.header = parts[0]
      this.payload = JSON.parse(atob(parts[1]))
      this.user = {
        username: this.payload.iss,
        profiles: this.payload.sub.split('&')
      }
      this.timestamp = time
    } else {
      this.jwt = null
      this.header = null
      this.payload = null
      this.user = null
      this.timestamp = null
    }
  }

  push (jwt) {
    this.__set(jwt, Date.now())
    if (this.jwt) {
      this.$localStorage.set('jwt', this.jwt)
      this.$localStorage.set('jwt.time', this.timestamp)
    } else {
      this.$localStorage.remove('jwt')
      this.$localStorage.remove('jwt.time')
    }
  }

  pull () {
    this.__set(this.$localStorage.get('jwt'), this.$localStorage.get('jwt.time'))
  }

  get remaining () {
    if (!this.timestamp || !this.payload) {
      return null
    }
    return (this.payload.exp - this.payload.iat) - Math.round((Date.now() - this.timestamp) / 1000)
  }

  get lifetime () {
    return (this.payload.exp - this.payload.iat)
  }
}
