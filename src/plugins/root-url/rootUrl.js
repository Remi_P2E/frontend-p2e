class Root {
  install (Vue) {
    let $rootUrl = {
      get () {
        let href = window.location.href
        return href.substr(0, href.length - this.$route.fullPath.length)
      }
    }
    Object.defineProperty(Vue, '$rootUrl', $rootUrl)
    Object.defineProperty(Vue.prototype, '$rootUrl', $rootUrl)
  }
}

export default new Root()
