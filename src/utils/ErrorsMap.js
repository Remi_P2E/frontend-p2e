import RecursiveArray from './RecursiveArray'

export default class ErrorsMap extends RecursiveArray {
  get (...args) {
    let value = super.get(...args)
    return value ? this.translate(value) : value
  }

  translate (value) {
    switch (value) {
      case 'This field is required': return 'Ce champ est requis'
      case 'Invalid value': return 'Valeur invalide'
      case 'Combinatory not compatible': return 'Combinatoire incompatible'
      default: return value
    }
  }
}
